import express from 'express'
import {
  countByCity,
  countByType,
  createHotel,
  deleteHotel,
  getHotel,
  getHotelRooms,
  getHotels,
  updateHotel,
} from '../controllers/hotelControllers.js'
import { verifyAdmin } from '../utils/verifyJwt.js'

const router = express.Router()

//CREATE
router.post('/', verifyAdmin, createHotel)

//READ ONE BY ID
router.get('/find/:id', getHotel)

//READ ALL
router.get('/', getHotels)

//UPDATE ONE
router.put('/:id', verifyAdmin, updateHotel)

//DELETE ONE
router.delete('/:id', verifyAdmin, deleteHotel)

//SUPPORTING ROUTES
router.get('/countByCity', countByCity)
router.get('/countByType', countByType)
router.get('/room/:id', getHotelRooms)

export default router
