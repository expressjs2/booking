import express from 'express'
import {
  createRoom,
  deleteRoom,
  getRoom,
  getRooms,
  reserve,
  updateRoom,
} from '../controllers/roomControllers.js'
import { verifyAdmin, verifyUser } from '../utils/verifyJwt.js'

const router = express.Router()

//CREATE
router.post('/:hotelId', verifyAdmin, createRoom)

//READ ONE
router.get('/:id', getRoom)

//READ MANY
router.get('/', getRooms)

//UPDATE ONE
router.put('/:id', verifyAdmin, updateRoom)

//DELETE ONE
router.delete('/:id/:hotelId', verifyAdmin, deleteRoom)

//SUPPORTING ROUTES

//UPDATE ROOM NUMBER UNAVAILABLE DATES
router.put('/reserve/:id', reserve)

export default router
