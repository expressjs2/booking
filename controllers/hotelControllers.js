import Hotel from '../models/Hotel.js'
import Room from '../models/Room.js'

//CREATE
export const createHotel = async (req, res, next) => {
  try {
    const newHotel = new Hotel(req.body)
    const savedHotel = await newHotel.save()
    res.status(200).json(savedHotel)
  } catch (error) {
    next(error)
  }
}

//READ ONE
export const getHotel = async (req, res, next) => {
  try {
    const hotel = await Hotel.findById(req.params.id)
    res.status(200).json(hotel)
  } catch (error) {
    next(error)
  }
}

//READ ALL
export const getHotels = async (req, res, next) => {
  const { min, max, limit, ...others } = req.query
  try {
    const hotels = await Hotel.find({
      ...others,
      cheapestPrice: { $gte: min ? min : 0, $lte: max || 99999999 },
    }).limit(limit)
    res.status(200).json(hotels)
  } catch (error) {
    next(error)
  }
}

//UPDATE ONE
export const updateHotel = async (req, res, next) => {
  try {
    const updatedHotel = await Hotel.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true } //true will return the updated obj to var
    )
    res.status(200).json(updatedHotel)
  } catch (error) {
    next(error)
  }
}

//DELETE ONE
export const deleteHotel = async (req, res, next) => {
  try {
    await Hotel.findByIdAndDelete(req.params.id)
    res.status(200).json('hotel successfully deleted.')
  } catch (error) {
    next(error)
  }
}

//SUPPORT FUNCTIONS

//COUNT BY CITY
export const countByCity = async (req, res, next) => {
  try {
    //return the query as an array. split the array by ','
    const cities = req.query.cities.split(',')
    const list = await Promise.all(
      cities.map((city) => {
        return Hotel.countDocuments({ city: city })
      })
    )

    res.status(200).json(list)
  } catch (error) {
    next(error)
  }
}

//COUNT BY TYPE
export const countByType = async (req, res, next) => {
  try {
    //return the query as an array. split the array by ','
    const hotelCount = await Hotel.countDocuments({ type: 'hotel' })
    const appartmentCount = await Hotel.countDocuments({ type: 'appartment' })
    const resortCount = await Hotel.countDocuments({ type: 'resort' })
    const villaCount = await Hotel.countDocuments({ type: 'villa' })
    const cabinCount = await Hotel.countDocuments({ type: 'cabin' })

    res.status(200).json([
      { type: 'hotel', count: hotelCount },
      { type: 'appartment', count: appartmentCount },
      { type: 'resort', count: resortCount },
      { type: 'villa', count: villaCount },
      { type: 'cabin', count: cabinCount },
    ])
  } catch (error) {
    next(error)
  }
}

//GET HOTEL ROOMS
export const getHotelRooms = async (req, res, next) => {
  try {
    const hotel = await Hotel.findById(req.params.id)
    const list = await Promise.all(
      hotel.rooms.map((room) => {
        return Room.findById(room)
      })
    )
    res.status(200).json(list)
  } catch (error) {
    next(error)
  }
}
