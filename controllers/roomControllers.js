import Room from '../models/Room.js'
import Hotel from '../models/Hotel.js'

//CREATE
export const createRoom = async (req, res, next) => {
  try {
    const newRoom = new Room(req.body)
    const savedRoom = await newRoom.save()

    try {
      await Hotel.findByIdAndUpdate(req.params.hotelId, {
        $push: { rooms: savedRoom._id },
      })
    } catch (error) {
      next(error)
    }
    res.status(200).json(savedRoom)
  } catch (error) {
    next(error)
  }
}

//READ ONE
export const getRoom = async (req, res, next) => {
  try {
    const room = await Room.findById(req.params.id)
    res.status(200).json(room)
  } catch (error) {
    next(error)
  }
}

//READ MANY
export const getRooms = async (req, res, next) => {
  try {
    const rooms = await Room.find()
    res.status(200).json(rooms)
  } catch (error) {
    next(error)
  }
}

//UPDATE ONE
export const updateRoom = async (req, res, next) => {
  try {
    const updatedRoom = await Room.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true } //return the updated version
    )
    res.status(200).json(updatedRoom)
  } catch (error) {
    next(error)
  }
}

//DELETE ONE
export const deleteRoom = async (req, res, next) => {
  try {
    await Room.findByIdAndDelete(req.params.id)

    try {
      await Hotel.findByIdAndUpdate(req.params.hotelId, {
        $pull: { rooms: req.params.id },
      })
    } catch (error) {
      next(error)
    }

    res.status(200).json('Room successfully deleted!')
  } catch (error) {
    next(error)
  }
}

//SUPPORTING ROUTES

/**RESERVE ROOM LOGIC
 * Note: The Term ROOM is LIKE a ROOM TYPE which can have many room numbers
 * 1. Find the room number id to push selected dates using updateOne()
 * 2. push the dates on unavailableDates property
 *
 * Syntax to push on nested property
 * roomNumbers:{"number":1, "unavailableDates":[Dates]}
 */
export const reserve = async (req, res, next) => {
  try {
    const dates = await Room.updateOne(
      { 'roomNumbers._id': req.params.id },
      { $push: { 'roomNumbers.$.unavailableDates': req.body.dates } }, //syntax to update nested property
      { new: true }
    )
    res.status(200).json(dates)
  } catch (error) {
    next(error)
  }
}
