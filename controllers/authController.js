import User from '../models/User.js'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { createError } from '../utils/error.js'

//REGISTER
export const register = async (req, res, next) => {
  try {
    const { username, email, password } = req.body

    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(password, salt)

    const newUser = new User({
      ...req.body,
      password: hash,
    })
    const createdUser = await newUser.save()
    res.status(200).json(createdUser)
  } catch (error) {
    next(error)
  }
}

//LOGIN
export const login = async (req, res, next) => {
  try {
    const { username, password } = req.body

    //check if username exits
    const user = await User.findOne({ username })

    //if no username found, exit and send error
    if (!user) {
      return next(createError(404, 'User not found!'))
    }

    //check if password provided is correct
    const isPasswordCorrect = await bcrypt.compare(password, user.password)

    //if password did not match, exit and send error
    if (!isPasswordCorrect) {
      return next(createError(400, 'Invalid credentials!'))
    }

    //send details as cookie via jwt
    const token = jwt.sign(
      { id: user._id, isAdmin: user.isAdmin },
      process.env.JWT_SECRET
    )

    //set received token into cookie

    //Destructure user object to select what is sent to client
    const { password: pwd, isAdmin, ...etc } = user._doc

    //send token to cookie also
    res
      .cookie('access_token', token, {
        httpOnly: true,
      }) //httpOnly:more safer
      .status(200)
      .json({ details: { ...etc }, isAdmin })
  } catch (error) {
    next(error)
  }
}
