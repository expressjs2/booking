import User from '../models/User.js'
import bcrypt from 'bcryptjs'

//CREATE
//handled by authController

//READ ONE
export const getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id)
    res.status(200).json(user)
  } catch (error) {
    next(error)
  }
}

export const getUsers = async (req, res, next) => {
  try {
    const users = await User.find()
    res.status(200).json(users)
  } catch (error) {
    next(error)
  }
}

//UPDATE
export const updateUser = async (req, res, next) => {
  // let hash
  try {
    const { username, email, isAdmin, city, country, phone } = req.body

    const user = await User.findById(req.params.id)

    // const isSamePassword = await bcrypt.compare(password, user.password)

    // if (!isSamePassword) {
    //   const salt = bcrypt.genSaltSync(10)
    //   hash = bcrypt.hashSync(password, salt)
    // }

    const updatedUser = await User.findByIdAndUpdate(
      req.params.id,
      {
        $set: {
          username,
          email,
          isAdmin,
          city,
          country,
          phone,
        },
      },
      { new: true }
    )
    res.status(200).json(updatedUser)
  } catch (error) {
    next(error)
  }
}

//UPDATE PASSWORD
export const updatePassword = async (req, res, next) => {
  try {
    const { password, newPassword } = req.body
    const user = await User.findById(req.params.id)
    const isPasswordCorrect = await bcrypt.compare(password, user.password)

    if (isPasswordCorrect) {
      const salt = bcrypt.genSaltSync(10)
      const hash = bcrypt.hashSync(newPassword, salt)

      await User.findByIdAndUpdate(
        req.params.id,
        { $set: { password: hash } },
        { new: true }
      )
      res.status(200).json('Password successfully updated.')
    } else {
      res.status(400).json('Incorrect password!')
    }
  } catch (error) {
    next(error)
  }
}

//DELETE
export const deleteUser = async (req, res, next) => {
  try {
    await User.findByIdAndDelete(req.params.id)
    res.status(200).json('User deleted!')
  } catch (error) {
    next(error)
  }
}
