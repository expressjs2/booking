import mongoose from 'mongoose'

/**Business Logic
 * Room can have the same title, price, maxPeople, desc
 * Room can only have one roomNumber
 * Room can have one or many unavailableDates once it is booked
 */
const roomSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    price: { type: Number, required: true },
    maxPeople: { type: Number, required: true },
    desc: { type: String, required: true },
    roomNumbers: [
      {
        number: { type: Number, required: true },
        unavailableDates: { type: [Date] },
      },
    ],
  },
  { timestamps: true }
)

export default mongoose.model('Room', roomSchema)
